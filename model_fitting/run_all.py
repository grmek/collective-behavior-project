import interpolate_data
import classify_data
import get_model_io_data
import estimate_model_parameters
import time

for i in range(3, 11):
    t0 = time.time()

    print(i, 1)
    interpolate_data.main([f'../data/zebrafish_raw/Homogeneous_10AB/10AB_homogeneous_{i:02}.txt',
                           f'../data/zebrafish_interpolated/10AB_homogeneous_{i:02}_%d_%d_%d.txt'])

    print(i, 2)
    classify_data.main([f'../data/zebrafish_interpolated/10AB_homogeneous_{i:02}_0_3600_%d.txt',
                        f'../data/zebrafish_classified/10AB_homogeneous_{i:02}_0_3600.txt'])

    print(i, 3)
    get_model_io_data.main([f'../data/zebrafish_interpolated/10AB_homogeneous_{i:02}_0_3600_%d.txt',
                            f'../data/zebrafish_classified/10AB_homogeneous_{i:02}_0_3600.txt',
                            f'../data/zebrafish_model_io/10AB_homogeneous_{i:02}_0_3600_%d.gz'])

    print(i, 4)
    estimate_model_parameters.main([f'../data/zebrafish_model_io/10AB_homogeneous_{i:02}_0_3600_%d.gz',
                                    f'../data/zebrafish_parameters/10AB_homogeneous_{i:02}_0_3600.txt'])

    print(time.time() - t0)
