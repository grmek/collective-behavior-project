import sys
import csv
import joblib
import numpy as np


def main(argv):
    file_path_in = argv[0]
    file_path_class = argv[1]
    file_path_out = argv[2]
    num_agents = 10

    # Read all agent positions into np array.
    agent_positions = []

    for i in range(num_agents):
        with open(file_path_in % i, 'r', newline='') as f:
            csv_reader = csv.reader(f, delimiter=' ')
            next(csv_reader)
            agent_positions.append(list(csv_reader))

    agent_positions = np.array(agent_positions).astype('float32')

    # Get v (model input), v_der_t and psi_der_t (model outputs).
    v_xy = (agent_positions[:, 1:, :] - agent_positions[:, :-1, :]) / 0.02
    v = (v_xy[:, :, 0] ** 2 + v_xy[:, :, 1] ** 2) ** 0.5
    psi = np.arctan2(v_xy[:, :, 0], v_xy[:, :, 1])
    v_der_t = (v[:, 1:] - v[:, :-1]) / 0.02
    psi_der_t = ((psi[:, 1:] - psi[:, :-1] + np.pi) % (2 * np.pi) - np.pi) / 0.02

    # Remove last v sample, as we don't know its v_der_t and psi_der_t.
    v = v[:, :-1]

    # Remove first and last position samples, as we don't know their v, v_der_t and psi_der_t.
    agent_positions = agent_positions[:, 1:-1, :]

    # Get V (model input) for remaining samples.
    V = np.zeros((num_agents, agent_positions.shape[1], 1025), dtype='float32')

    for a in range(V.shape[0]):
        print('Processing agent', a + 1, '/', V.shape[0])
        for t in range(V.shape[1]):
            for a_ in range(V.shape[0]):
                if a_ != a:
                    relative_position = agent_positions[a_, t, :] - agent_positions[a, t, :]
                    distance = (relative_position[0] ** 2 + relative_position[1] ** 2) ** 0.5
                    phi = np.arctan2(relative_position[0], relative_position[1]) - psi[a, t]
                    radius_angle = np.arctan2(0.5, distance)

                    start_idx = int(np.rint((phi - radius_angle) / (2.0 * np.pi) * 1025))
                    stop_idx = int(np.rint((phi + radius_angle) / (2.0 * np.pi) * 1025))

                    for i in range(start_idx, stop_idx + 1):
                        V[a, t, i % 1025] = 1.0

    # Write model inputs and outputs to a file.
    joblib.dump((v, V, v_der_t, psi_der_t), file_path_out % 0)

    # Classify motion.
    with open(file_path_class, 'r', newline='') as f:
        csv_reader = csv.reader(f, delimiter=' ')
        next(csv_reader)
        pg_mg = np.array(list(csv_reader))[:-2, :-1].astype('float32')
        pg = pg_mg[:, 0]
        mg = pg_mg[:, 1]

    motion_class = np.zeros((v.shape[1]), dtype='int')
    motion_class[np.logical_and(pg < 0.5, mg < 0.8)] = 1
    motion_class[np.logical_and(pg < 0.35, mg >= 0.8)] = 2
    motion_class[np.logical_and(pg >= 0.8, mg >= 0.8)] = 3
    motion_class[np.logical_and(pg >= 0.8, np.logical_and(mg >= 0.2, mg < 0.8))] = 4
    motion_class[np.logical_and(pg >= 0.8, mg < 0.2)] = 5

    # Write model inputs and outputs to a file.
    for i in range(1, 6):
        mask = (motion_class == i)
        joblib.dump((v[:, mask], V[:, mask, :], v_der_t[:, mask], psi_der_t[:, mask]), file_path_out % i)


if __name__ == '__main__':
    main(sys.argv[1:])
