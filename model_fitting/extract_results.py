def main():
    for i in range(1, 11):
        with open(f'../data/zebrafish_parameters/10AB_homogeneous_{i:02d}_0_3600.txt') as f:
            lines = f.readlines()
            start = 5
            for j in range(6):
                try:
                    print(lines[start + j * 69][12:-1].replace('.', ','), end='\t')
                except:
                    print('/', end='\t')
            print()


if __name__ == '__main__':
    main()
