import numpy as np


def main(argv):
    file_path_in = argv[0]
    file_path_out = argv[1]
    Classify(file_path_in, file_path_out)


#NOT USED (we classify behavior only in unity script. That way we only have to change 1 thing whenever we change behavior classification model)
def getBehavior(pG, mG):
    if pG < 0.4 and mG < 0.4:
        return "Swarm"
    elif pG < 0.4 and mG > 0.6:
        return "Circling"
    elif pG > 0.8 and mG < 0.4:
        return "Highly parallel"
    elif pG > 0.6 and mG < 0.4:
        return "Parallel"
    else:
        return "Undefined"
    

def Classify(dir_in, dir_out):
    numOfFish=10
    ListOfPositions = list()
    ListOfpGroup = list()
    ListOfmGroup = list()
    #Read everything and save into ListOfPositions (ListOfPosition[0] ~ data for first fish, ListOfPosition[0][0] ~ first X and Y of fish 1)
    for a in range(numOfFish):
        f = open(dir_in % a)
        lines = int(f.readline())
        howMany = lines
        ListOfCurrentPosition = list()
        for unused in range(lines):
            c = f.readline().split(" ")
            ListOfCurrentPosition.append(np.array(list(map(float, c))))
        ListOfPositions.append(ListOfCurrentPosition)
    
    #For each setting of data (we assume all fish have the same amount of data)
    for p in range(howMany-1):
        #Reset pGroup and mGroup
        sumpGroup = np.array([0,0])
        summGroup = np.array([0]) #This is only one param because np.cross return array of length 1 if both vectors are of length 2
        #Calculate center
        center = np.array([0,0])
        for f in range(numOfFish):
            center = center + (ListOfPositions[f][p] / numOfFish)
        #Calculate pGroup and mGroup
        for f in range(numOfFish):
            curDir = ListOfPositions[f][p+1]-ListOfPositions[f][p]
            curDir = curDir / np.linalg.norm(curDir)
            sumpGroup = sumpGroup + curDir
            curDisFromCenter = ListOfPositions[f][p] - center
            summGroup = summGroup + np.absolute(np.cross(curDisFromCenter / np.linalg.norm(curDisFromCenter), curDir))
        #The finishing calcuations for pGroup and mGroup
        sumpGroup = np.linalg.norm(sumpGroup / numOfFish)
        summGroup = np.linalg.norm(summGroup / numOfFish)
        #Add the results to the list
        ListOfpGroup.append(sumpGroup)
        ListOfmGroup.append(summGroup)
    #We duplicate the last element, so we have the same amount of mGroup and pGroup as we have data
    ListOfpGroup.append(ListOfpGroup[-1])
    ListOfmGroup.append(ListOfmGroup[-1])
    f = open(dir_out, "w+")
    f.write(str(howMany))
    for i in range(howMany):
        f.write("\n" + str(ListOfpGroup[i]) + " " + str(ListOfmGroup[i]) + " ")
    #return [ListOfPositions,ListOfpGroup, ListOfmGroup]


#di = "C:\\Users\\Asus\\Desktop\\Sola\\2020_21-1\\Skupinsko vedenje\\Prr\\data\\"
#a=Classify(di


if __name__ == '__main__':
    main(sys.argv[1:])
