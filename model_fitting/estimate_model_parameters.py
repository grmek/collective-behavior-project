import sys
import joblib
import numpy as np
import scipy


def main(argv):
    file_path_in = argv[0]
    file_path_out = argv[1]

    with open(file_path_out, 'w') as f:
        for i in range(6):
            v, V, v_der_t, psi_der_t = joblib.load(file_path_in % i)

            # We can't process more than 40000 samples, therefore we will dynamically down-sample to every k-th sample.
            n = v.shape[1]
            k = 1 + n // 40000
            n_ds = n // k

            f.write('Motion class %d (%d samples, down-sampled to %d):\n' % (i, n, n_ds))

            if n_ds > 0:
                fit_reference_model(v[:, ::k], V[:, ::k, :], v_der_t[:, ::k], psi_der_t[:, ::k], f)
                fit_general_model(v[:, ::k], V[:, ::k, :], v_der_t[:, ::k], psi_der_t[:, ::k], f)
                fit_reference_model_with_blind_zone(v[:, ::k], V[:, ::k, :], v_der_t[:, ::k], psi_der_t[:, ::k], f)
                fit_general_model_with_blind_zone(v[:, ::k], V[:, ::k, :], v_der_t[:, ::k], psi_der_t[:, ::k], f)
            else:
                f.write('  No samples in this class.')


def fit_reference_model(v, V, v_der_t, psi_der_t, f):
    f.write('  Fitting the reference model:\n')

    V_der_phi_sqrd = (V - np.roll(V, 1, axis=2)) ** 2

    prev_V = np.roll(V, 1, axis=1)
    prev_V[:, 0, :] = 0
    V_der_t = V - prev_V

    cos_phi_n = np.cos(np.arange(V.shape[2]) / V.shape[2] * 2 * np.pi)

    A = np.zeros((v.shape[0] * v.shape[1], 5), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 1
            A[row, 1] = v[a, t]
            A[row, 2] = 2 * np.pi / V.shape[2] * -V[a, t, :].dot(cos_phi_n)
            A[row, 3] = 2 * np.pi / V.shape[2] * V_der_phi_sqrd[a, t, :].dot(cos_phi_n)
            A[row, 4] = 2 * np.pi / V.shape[2] * V_der_t[a, t, :].dot(cos_phi_n)

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = v_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation v_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      v0 = %f\n' % (-x[0] / x[1]))
    f.write('      gamma = %f\n' % -x[1])
    f.write('      alpha0 = %f\n' % x[2])
    f.write('      alpha1 = %f\n' % (x[3] / x[2]))
    f.write('      alpha2 = %f\n' % (x[4] / x[2]))

    sin_phi_n = np.sin(np.arange(V.shape[2]) / V.shape[2] * 2 * np.pi)

    A = np.zeros((v.shape[0] * v.shape[1], 3), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 2 * np.pi / V.shape[2] * -V[a, t, :].dot(sin_phi_n)
            A[row, 1] = 2 * np.pi / V.shape[2] * V_der_phi_sqrd[a, t, :].dot(sin_phi_n)
            A[row, 2] = 2 * np.pi / V.shape[2] * V_der_t[a, t, :].dot(sin_phi_n)

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = psi_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation psi_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      beta0 = %f\n' % x[0])
    f.write('      beta1 = %f\n' % (x[1] / x[0]))
    f.write('      beta2 = %f\n' % (x[2] / x[0]))


def fit_general_model(v, V, v_der_t, psi_der_t, f):
    f.write('  Fitting the general model:\n')

    V_der_phi_sqrd = (V - np.roll(V, 1, axis=2)) ** 2

    prev_V = np.roll(V, 1, axis=1)
    prev_V[:, 0, :] = 0
    V_der_t = V - prev_V

    A = np.zeros((v.shape[0] * v.shape[1], 2 + 3 * V.shape[2]), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 1
            A[row, 1] = v[a, t]
            A[row, (2 + 0 * V.shape[2]):(2 + 1 * V.shape[2])] = -V[a, t, :]
            A[row, (2 + 1 * V.shape[2]):(2 + 2 * V.shape[2])] = V_der_phi_sqrd[a, t, :]
            A[row, (2 + 2 * V.shape[2]):(2 + 3 * V.shape[2])] = V_der_t[a, t, :]

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = v_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation v_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      v0 = %f\n' % (-x[0] / x[1]))
    f.write('      gamma = %f\n' % -x[1])
    f.write('      lambda0 = %s\n' % list(x[(2 + 0 * V.shape[2]):(2 + 1 * V.shape[2]), 0]))
    f.write('      lambda1 = %s\n' % list(x[(2 + 1 * V.shape[2]):(2 + 2 * V.shape[2]), 0]))
    f.write('      lambda2 = %s\n' % list(x[(2 + 2 * V.shape[2]):(2 + 3 * V.shape[2]), 0]))

    A = np.zeros((v.shape[0] * v.shape[1], 3 * V.shape[2]), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, (0 * V.shape[2]):(1 * V.shape[2])] = -V[a, t, :]
            A[row, (1 * V.shape[2]):(2 * V.shape[2])] = V_der_phi_sqrd[a, t, :]
            A[row, (2 * V.shape[2]):(3 * V.shape[2])] = V_der_t[a, t, :]

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = psi_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation psi_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      mu0 = %s\n' % list(x[(0 * V.shape[2]):(1 * V.shape[2]), 0]))
    f.write('      mu1 = %s\n' % list(x[(1 * V.shape[2]):(2 * V.shape[2]), 0]))
    f.write('      mu2 = %s\n' % list(x[(2 * V.shape[2]):(3 * V.shape[2]), 0]))


def fit_reference_model_with_blind_zone(v, V, v_der_t, psi_der_t, f):
    f.write('  Fitting the reference model with blind zone:\n')

    V[:, :, 384:641] = 0

    V_der_phi_sqrd = (V - np.roll(V, 1, axis=2)) ** 2

    prev_V = np.roll(V, 1, axis=1)
    prev_V[:, 0, :] = 0
    V_der_t = V - prev_V

    cos_phi_n = np.cos(np.arange(V.shape[2]) / V.shape[2] * 2 * np.pi)

    A = np.zeros((v.shape[0] * v.shape[1], 5), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 1
            A[row, 1] = v[a, t]
            A[row, 2] = 2 * np.pi / V.shape[2] * -V[a, t, :].dot(cos_phi_n)
            A[row, 3] = 2 * np.pi / V.shape[2] * V_der_phi_sqrd[a, t, :].dot(cos_phi_n)
            A[row, 4] = 2 * np.pi / V.shape[2] * V_der_t[a, t, :].dot(cos_phi_n)

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = v_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation v_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      v0 = %f\n' % (-x[0] / x[1]))
    f.write('      gamma = %f\n' % -x[1])
    f.write('      alpha0 = %f\n' % x[2])
    f.write('      alpha1 = %f\n' % (x[3] / x[2]))
    f.write('      alpha2 = %f\n' % (x[4] / x[2]))

    sin_phi_n = np.sin(np.arange(V.shape[2]) / V.shape[2] * 2 * np.pi)

    A = np.zeros((v.shape[0] * v.shape[1], 3), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 2 * np.pi / V.shape[2] * -V[a, t, :].dot(sin_phi_n)
            A[row, 1] = 2 * np.pi / V.shape[2] * V_der_phi_sqrd[a, t, :].dot(sin_phi_n)
            A[row, 2] = 2 * np.pi / V.shape[2] * V_der_t[a, t, :].dot(sin_phi_n)

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = psi_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation psi_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      beta0 = %f\n' % x[0])
    f.write('      beta1 = %f\n' % (x[1] / x[0]))
    f.write('      beta2 = %f\n' % (x[2] / x[0]))


def fit_general_model_with_blind_zone(v, V, v_der_t, psi_der_t, f):
    f.write('  Fitting the general model with blind zone:\n')

    V[:, :, 384:641] = 0

    V_der_phi_sqrd = (V - np.roll(V, 1, axis=2)) ** 2

    prev_V = np.roll(V, 1, axis=1)
    prev_V[:, 0, :] = 0
    V_der_t = V - prev_V

    A = np.zeros((v.shape[0] * v.shape[1], 2 + 3 * V.shape[2]), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, 0] = 1
            A[row, 1] = v[a, t]
            A[row, (2 + 0 * V.shape[2]):(2 + 1 * V.shape[2])] = -V[a, t, :]
            A[row, (2 + 1 * V.shape[2]):(2 + 2 * V.shape[2])] = V_der_phi_sqrd[a, t, :]
            A[row, (2 + 2 * V.shape[2]):(2 + 3 * V.shape[2])] = V_der_t[a, t, :]

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = v_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation v_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      v0 = %f\n' % (-x[0] / x[1]))
    f.write('      gamma = %f\n' % -x[1])
    f.write('      lambda0 = %s\n' % list(x[(2 + 0 * V.shape[2]):(2 + 1 * V.shape[2]), 0]))
    f.write('      lambda1 = %s\n' % list(x[(2 + 1 * V.shape[2]):(2 + 2 * V.shape[2]), 0]))
    f.write('      lambda2 = %s\n' % list(x[(2 + 2 * V.shape[2]):(2 + 3 * V.shape[2]), 0]))

    A = np.zeros((v.shape[0] * v.shape[1], 3 * V.shape[2]), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            A[row, (0 * V.shape[2]):(1 * V.shape[2])] = -V[a, t, :]
            A[row, (1 * V.shape[2]):(2 * V.shape[2])] = V_der_phi_sqrd[a, t, :]
            A[row, (2 * V.shape[2]):(3 * V.shape[2])] = V_der_t[a, t, :]

    b = np.zeros((v.shape[0] * v.shape[1], 1), dtype='float32')

    for a in range(v.shape[0]):
        for t in range(v.shape[1]):
            row = a * v.shape[1] + t
            b[row, 0] = psi_der_t[a, t]

    x, _, _, _ = scipy.linalg.lstsq(A, b)
    b_ = A.dot(x)

    f.write('    Equation psi_der_t:\n')
    f.write('      Ground-truth output data (min, avg, max): %f %f %f\n' % (np.min(b), np.average(b), np.max(b)))
    f.write('      Predicted output data (min, avg, max): %f %f %f\n' % (np.min(b_), np.average(b_), np.max(b_)))
    f.write('      RMSE: %f\n' % np.average((b - b_) ** 2) ** 0.5)
    f.write('      mu0 = %s\n' % list(x[(0 * V.shape[2]):(1 * V.shape[2]), 0]))
    f.write('      mu1 = %s\n' % list(x[(1 * V.shape[2]):(2 * V.shape[2]), 0]))
    f.write('      mu2 = %s\n' % list(x[(2 * V.shape[2]):(3 * V.shape[2]), 0]))


if __name__ == '__main__':
    main(sys.argv[1:])
