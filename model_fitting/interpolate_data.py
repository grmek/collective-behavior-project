import sys
import csv
import numpy as np
from scipy import interpolate
# import matplotlib.pyplot as plt


def main(argv):
    file_path_in = argv[0]
    file_path_out = argv[1]
    first_sample = 0
    last_sample = 3600
    scaling = 100 / 1.2
    translation_x = -50
    translation_y = -50

    with open(file_path_in, 'r', newline='') as f:
        csv_reader = csv.reader(f, delimiter=' ')
        next(csv_reader)
        data = np.array(list(csv_reader)).astype('float32')

    t = data[first_sample:last_sample, 0]
    t_new = np.arange(t[0], t[-1], 0.02)

    for i in range((data.shape[1] - 1) // 2):
        x = data[first_sample:last_sample, (1 + 2 * i)] * scaling + translation_x
        y = data[first_sample:last_sample, (1 + 2 * i + 1)] * scaling + translation_y
        tck, _ = interpolate.splprep([x, y], u=t, k=3, s=0)
        x_new, y_new = interpolate.splev(t_new, tck)

        # plt.plot(x_new, y_new, 'k', x, y, 'ko')
        # plt.show()

        with open(file_path_out % (first_sample, last_sample, i), 'w', newline='') as f:
            f.write(str(len(t_new)) + '\n')
            csv_writer = csv.writer(f, delimiter=' ')
            for row in zip(x_new, y_new):
                csv_writer.writerow(row)


if __name__ == '__main__':
    main(sys.argv[1:])
