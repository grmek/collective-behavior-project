using UnityEngine;

public abstract class Agent
{
    public abstract void UpdateMotion(Agent[] agents);

    public abstract void UpdatePosition();

    public abstract Vector3 GetPosition();

    public abstract Vector3 GetDirectionVector();

    protected GameObject createGameObject()
    {
        GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        SphereCollider sphereCollider = gameObject.GetComponent<SphereCollider>();
        sphereCollider.enabled = false;

        Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY;

        Renderer renderer = gameObject.GetComponent<Renderer>();
        renderer.material = new Material(Shader.Find("Standard"));
        renderer.material.color = Color.black;

        TrailRenderer trailRenderer = gameObject.AddComponent<TrailRenderer>();
        trailRenderer.time = 2.5f;
        trailRenderer.startWidth = 0.3f;
        trailRenderer.endWidth = 0.0f;
        trailRenderer.material.color = Color.black;

        return gameObject;
    }
}
