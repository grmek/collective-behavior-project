﻿using UnityEngine;
using System.IO;
using System.Globalization;

public class DataAgent : Agent
{
    private float bodyLength;
    private float deltaTime;

    private float[] dataX;
    private float[] dataY;

    private float startTime;

    private GameObject gameObject;

    public DataAgent(float bl, float dt, string fileName, float sTime)
    {
        bodyLength = bl;
        deltaTime = dt;

        LoadData(fileName);

        startTime = sTime;

        gameObject = createGameObject();
        gameObject.transform.position = new Vector3(dataX[0], 0.0f, dataY[0]);
        gameObject.transform.localScale = new Vector3(bodyLength, bodyLength, bodyLength);
    }

    public override void UpdateMotion(Agent[] agents)
    {
        /* We don't do anything here, as we are directly updating positions. */
    }

    public override void UpdatePosition()
    {
        int idx = Mathf.CeilToInt((Time.time - startTime) / deltaTime);

        if (idx < dataX.Length)
        {
            Vector3 position = GetPosition();

            position[0] = dataX[idx];
            position[2] = dataY[idx];

            gameObject.transform.position = position;
        }
    }

    public override Vector3 GetPosition()
    {
        return gameObject.transform.position;
    }


    private void LoadData(string fileName) 
    {
        StreamReader reader = new StreamReader(fileName);

        int numElements = int.Parse(reader.ReadLine());
        dataX = new float[numElements];
        dataY = new float[numElements];

        for (int i = 0; i < numElements; i++)
        {
            string[] strArr = reader.ReadLine().Split(' ');
            dataX[i] = float.Parse(strArr[0], CultureInfo.InvariantCulture);
            dataY[i] = float.Parse(strArr[1], CultureInfo.InvariantCulture);
        }

        reader.Close();
    }

    public override Vector3 GetDirectionVector() 
    {
        int idx = Mathf.CeilToInt((Time.time - startTime) / deltaTime);

        /* We have no more data, so the direction vector is (0,0,0) */
        if (idx >= this.dataX.Length) {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }
        return new Vector3((dataX[idx+1]-dataX[idx]), 0.0f, (dataY[idx + 1] - dataY[idx]));

    }
}
