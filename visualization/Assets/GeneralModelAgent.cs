﻿using UnityEngine;

public class GeneralModelAgent : Agent
{
    private float bodyLength;

    private float vZero;
    private float gamma;

    private float[] lambda0;
    private float[] lambda1;
    private float[] lambda2;

    private float[] mu0;
    private float[] mu1;
    private float[] mu2;

    private float deltaTime;
    private int resolution;

    private GameObject gameObject;

    private float v;
    private float psi;

    private float[] visualProjField;

    public GeneralModelAgent(float bl,
                             float v0, float g,
                             float[] l0, float[] l1, float[] l2,
                             float[] m0, float[] m1, float[] m2,
                             float dt, int r)
    {
        bodyLength = bl;
        vZero = v0;
        gamma = g;
        lambda0 = l0;
        lambda1 = l1;
        lambda2 = l2;
        mu0 = m0;
        mu1 = m1;
        mu2 = m2;
        deltaTime = dt;
        resolution = r;

        gameObject = createGameObject();
        gameObject.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), 0.0f, Random.Range(-5.0f, 5.0f));
        gameObject.transform.localScale = new Vector3(bodyLength, bodyLength, bodyLength);

        v = Random.Range(0.0f, vZero);
        psi = Random.Range(-Mathf.PI, Mathf.PI);

        visualProjField = new float[resolution];
    }

    public override void UpdateMotion(Agent[] agents)
    {
        float[] V = GetVisualProjField(agents);

        float[] VDerPhiSqrd = new float[resolution];
        float[] VDerT = new float[resolution];

        for (int i = 0; i < resolution; i++)
        {
            VDerPhiSqrd[i] = Mathf.Pow((V[i] - V[(i - 1 + resolution) % resolution]), 2.0f);
            VDerT[i] = V[i] - visualProjField[i];
        }

        float deltaPhi = 2.0f * Mathf.PI / resolution;
        float integralV = 0.0f;
        float integralPsi = 0.0f;

        for (int i = 0; i < resolution; i++)
        {
            float phi = (float)i / resolution * 2.0f * Mathf.PI;
            integralV += lambda0[i] * (-V[i]) + lambda1[i] * VDerPhiSqrd[i] + lambda2[i] * VDerT[i];
            integralPsi += mu0[i] * (-V[i]) + mu1[i] * VDerPhiSqrd[i] + mu2[i] * VDerT[i];
        }

        float vDerT = gamma * (vZero - v) + integralV;
        float psiDerT = integralPsi;

        v += vDerT * deltaTime;
        psi += psiDerT * deltaTime;
        
        visualProjField = V;
    }

    public override void UpdatePosition()
    {
        Vector3 position = GetPosition();

        position[0] += v * Mathf.Sin(psi) * deltaTime;
        position[2] += v * Mathf.Cos(psi) * deltaTime;

        gameObject.transform.position = position;
    }

    public override Vector3 GetPosition()
    {
        return gameObject.transform.position;
    }

    private float[] GetVisualProjField(Agent[] agents)
    {
        float[] field = new float[resolution];

        Vector3 position = GetPosition();

        foreach (Agent a in agents)
        {
            if (a != this)
            {
                Vector3 relativePosition = a.GetPosition() - position;
                float distance = relativePosition.magnitude;
                float phi = Mathf.Atan2(relativePosition[0], relativePosition[2]) - psi;
                float radiusAngle = Mathf.Atan2((bodyLength * 0.5f), distance);

                int startPosition = Mathf.RoundToInt((phi - radiusAngle) / (2.0f * Mathf.PI) * resolution);
                int stopPosition = Mathf.RoundToInt((phi + radiusAngle) / (2.0f * Mathf.PI) * resolution);

                for (int i = startPosition; i <= stopPosition; i++)
                {
                    field[((i % resolution) + resolution) % resolution] = 1.0f;
                }
            }
        }

        return field;
    }

    public override Vector3 GetDirectionVector()
    {
        return new Vector3(Mathf.Sin(this.psi), 0.0f, Mathf.Cos(this.psi));
    }

}
