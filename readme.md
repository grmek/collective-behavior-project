# Vision-based collective behavior modeling
Authors: Rok Grmek, Jakob Gaberc Artenjak

## What is this?
Vision-based collective behavior modeling is an open field that allows us to explore the direct impact of visual perception of the environment on the responses of individual agents. We implemented an existing model, presented in the article (Bastien & Romanczuk, 2020, DOI: 10.1126/sciadv.aay0792), and performed several experiments to check whether the model is suitable for modeling real data. We also presented a generalization of the model, which serves mainly as a tool for analyzing the reference model. The reference model performed well in most cases and did not significantly deviate from the general model, but we also noticed some shortcomings.

## How to run the experiments?
All models are implemented in Unity. The Unity project is saved in the *visualization* directory. Models and their parameters can be configured in the *AgentManager* class.

Scripts for model fitting experiments are available in the *model_fitting* directory. Use the script *run_all.py* to reproduce all the results.
